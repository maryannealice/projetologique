package br.com.logique.dao;

import java.util.ArrayList;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.Session;

import br.com.logique.config.Hibernate;
import br.com.logique.model.Employee;
import br.com.logique.model.Responsible;

public class EmployeeDAO {
	
	private Session session;
	
	public EmployeeDAO() {
		this.session = Hibernate.getSession();
		
	}
	
	public void insertEmployee(Employee emp) {
		try {
			Hibernate.beginTransaction();
			Hibernate.getSession().save(emp);
			Hibernate.commitTransaction();
		} catch (NoResultException e) {
			Hibernate.rollbackTransaction();
			e.printStackTrace();
		} finally {
			this.session.close();
		}
	}
	
	public Employee validation(Employee emp) {
		Employee employee = null;
		try {
			Hibernate.beginTransaction();

			TypedQuery<Employee> query = this.session.createQuery("from Employee e"
					+ " where e.login = :l and e.password = :p", Employee.class);
			query.setParameter("l", emp.getLogin());
			query.setParameter("p", emp.getPassword());
			employee = query.getSingleResult();
			
			Hibernate.commitTransaction();
		} catch (NoResultException ex) {
			ex.printStackTrace();
		} finally {
			this.session.close();
		}
		return employee;
	}

	public ArrayList<Employee> listEmployee() {
		ArrayList<Employee> employee = new ArrayList<Employee>();
		try {
			Hibernate.beginTransaction();
			
			TypedQuery<Employee> query = this.session.createQuery("from Employee r", Employee.class);
			employee = (ArrayList<Employee>) query.getResultList();
			
			Hibernate.commitTransaction();
		} catch (NoResultException e) {
			e.printStackTrace();
		} finally {
			this.session.close();
		}
		return employee;
	}
	
}