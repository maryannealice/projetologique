package br.com.logique.dao;

import javax.persistence.NoResultException;
import javax.ws.rs.Path;

import org.hibernate.Session;

import br.com.logique.config.Hibernate;
import br.com.logique.model.Dependent;

public class DependentDAO {
	
	private Session session;
	
	public DependentDAO() {
		this.session = Hibernate.getSession();
	}
	
	public void insertDependent(Dependent d) {
		try {
			Hibernate.beginTransaction();
			Hibernate.getSession().save(d);
			Hibernate.commitTransaction();
		} catch (NoResultException e) {
			e.printStackTrace();
		} finally {
			this.session.close();
		}
		
	}

}
