package br.com.logique.dao;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.ws.rs.Path;

import org.hibernate.Session;

import br.com.logique.config.Hibernate;
import br.com.logique.model.Associate;

public class AssociateDAO {

	private Session session;
	
	public AssociateDAO() {
		this.session = Hibernate.getSession();
	}
	
	public void insertAssociate(Associate a) {
		try {
			Hibernate.beginTransaction();
			Hibernate.getSession().save(a);		
			System.out.println(a);
			Hibernate.commitTransaction();
		} catch (NoResultException e) {
			e.printStackTrace();
		} finally {
			this.session.close();
		}
		
	}
	
}
