package br.com.logique.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.Session;

import br.com.logique.config.Hibernate;
import br.com.logique.model.Responsible;

public class ResponsibleDAO {
	
	private Session session;
	
	public ResponsibleDAO() {
		this.session = Hibernate.getSession();
	}

	public void insertResponsible(Responsible r) {
		try {
			Hibernate.beginTransaction();
			Hibernate.getSession().save(r);
			System.out.println(r);
			Hibernate.commitTransaction();
		} catch (NoResultException e) {
			e.printStackTrace();
		} finally {
			this.session.close();
		}
		
	}
	
	public List<Responsible> listResponsible() {
		List<Responsible> responsibles = new ArrayList<>();
		try {
			Hibernate.beginTransaction();
			TypedQuery<Responsible> query = this.session.createQuery("from Responsible r", Responsible.class);
			responsibles = (List<Responsible>) query.getResultList();
			Hibernate.commitTransaction();
		} catch (NoResultException e) {
			e.printStackTrace();
		} finally {
			this.session.close();
		}
		return responsibles;
	}
	
}