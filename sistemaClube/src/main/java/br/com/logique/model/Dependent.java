package br.com.logique.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="dependent")
@SequenceGenerator(name = "id_dependent", sequenceName = "id_seq_dependent", initialValue = 1, allocationSize = 1)
public class Dependent {
	
	public int id_dependent, id_responsible;
	
	public Associate associate;

	public Dependent() {  }
	
	@Id
	@Column(name = "id_dependent")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="id_dependent")
	public int getId_dependent() {
		return id_dependent;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_associate", nullable=false, updatable = false, insertable = true)
	public Associate getAssociate() {
		 return associate;
	}
	
	@Column(name="id_responsable")
	public int getId_responsible() {
		return id_responsible;
	}

	public void setId_responsible(int id_responsible) {
		this.id_responsible = id_responsible;
	}

	public void setId_dependent(int id_dependent) {
		this.id_dependent = id_dependent;
	}
	
	public void setAssociate(Associate associate) {
		this.associate = associate;
	}
}