package br.com.logique.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "associate")
@SequenceGenerator(name = "id_associate", sequenceName = "id_seq_associate", initialValue = 1, allocationSize = 1)
public class Associate {

	public String name, street, number, zip_code, city, state, phone1,
			phone2, type_associate, rg, cpf;

	public int id_associate;
	
	public Associate() { }

	public Responsible responsible;
    
	@OneToOne(mappedBy = "associate",cascade=CascadeType.ALL, fetch=FetchType.EAGER)     	 
	public Responsible getResponsible() {
		return responsible;
	}

	public void setResponsible(Responsible responsible) {
		this.responsible = responsible;
	}
	
	
	public Dependent dependent;
	
	@OneToOne(mappedBy = "associate",cascade=CascadeType.ALL, fetch=FetchType.LAZY)     	 
	public Dependent getDependent() {
		return dependent;
	}

	public void setDependent(Dependent dependent) {
		this.dependent = dependent;
	}
	
	
	@Id
	@Column(name = "id_associate")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="id_associate")
	public int getId_associate() {
		return id_associate;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}

	@Column(name="street")
	public String getStreet() {
		return street;
	}
	
	@Column(name="number")
	public String getNumber() {
		return number;
	}

	@Column(name="zip_code")
	public String getZip_code() {
		return zip_code;
	}
	
	
	@Column(name="city")
	public String getCity() {
		return city;
	}

	@Column(name="state")
	public String getState() {
		return state;
	}
	
	@Column(name="phone1")
	public String getPhone1() {
		return phone1;
	}

	@Column(name="phone2")
	public String getPhone2() {
		return phone2;
	}

	@Column(name="rg")
	public String getRg() {
		return rg;
	}

	@Column(name="cpf")
	public String getCpf() {
		return cpf;
	}
	
	@Column(name="type_associate")
	public String getType_associate() {
		return type_associate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public void setId_associate(int id_associate) {
		this.id_associate = id_associate;
	}

	public void setType_associate(String type_associate) {
		this.type_associate = type_associate;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}