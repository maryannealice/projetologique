package br.com.logique.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.logique.dao.DependentDAO;
import br.com.logique.model.Dependent;

@Path("/d")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DependentService {

	DependentDAO dao = new DependentDAO();
	
	@Path("/insertDependent")
	@POST
	public void insertDependent(Dependent d) {
		dao.insertDependent(d);
	}
	
}
