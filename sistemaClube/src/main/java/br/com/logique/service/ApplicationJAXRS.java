package br.com.logique.service;


import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.jackson.JacksonFeature;

import br.com.logique.utils.CORSFilter;


@ApplicationPath("/app") 
public class ApplicationJAXRS extends Application {
	
	public Set<Class<?>> getClasses() {
        Set<Class<?>> set = new HashSet<Class<?>>();
        set.add(AssociateService.class);
        set.add(DependentService.class);
        set.add(EmployeeService.class);
        set.add(ResponsibleService.class);
        return set;
    }
	
	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<Object>();
		singletons.add(new JacksonFeature());
		singletons.add(new CORSFilter());
		return singletons;
	}


}
