package br.com.logique.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import br.com.logique.dao.AssociateDAO;
import br.com.logique.model.Associate;

@Path("/a")
public class AssociateService {
	
	AssociateDAO dao = new AssociateDAO();

	@Path("/insertAssociate")
	@POST
	public void insertAssociate(Associate a) {
		dao.insertAssociate(a);
	}
	
}