package br.com.logique.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.logique.dao.ResponsibleDAO;
import br.com.logique.model.Responsible;

@Path("/r")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ResponsibleService {
	
	ResponsibleDAO dao = new ResponsibleDAO();

	@Path("/insertResponsible")
	@POST
	public void insertResponsible(Responsible r) {
		dao.insertResponsible(r);
	}
	
	@Path("/listResponsible")
	@GET
	public ArrayList<Responsible> listResponsible() {
		return dao.listResponsible();
	}
	
}
