package br.com.logique.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Dependent.class)
public abstract class Dependent_ {

	public static volatile SingularAttribute<Dependent, Integer> id_responsible;
	public static volatile SingularAttribute<Dependent, Integer> id_dependent;
	public static volatile SingularAttribute<Dependent, Associate> associate;

}

