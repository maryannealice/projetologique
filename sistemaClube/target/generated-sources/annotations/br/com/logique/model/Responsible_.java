package br.com.logique.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Responsible.class)
public abstract class Responsible_ {

	public static volatile SingularAttribute<Responsible, Integer> id_responsible;
	public static volatile SingularAttribute<Responsible, String> bank;
	public static volatile SingularAttribute<Responsible, String> agency;
	public static volatile SingularAttribute<Responsible, String> count;
	public static volatile SingularAttribute<Responsible, Associate> associate;

}

