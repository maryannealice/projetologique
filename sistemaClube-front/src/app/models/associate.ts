export class Associate {

    private name: string;
    private cpf: string;
    private rg: string;
    private type_associate: string;
    private phone1: string;
    private phone2: string;
    private street: string;
    private number: string;
    private zip_code: string;
    private city: string;
    private state: string;

    constructor() {  }
}