import { Component, OnInit } from '@angular/core';
import { UserLogin } from '../models/userLogin';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    user: UserLogin;
    statusLog: string = "false";

  constructor(private loginService: LoginService, private router: Router) { 
    this.user = new UserLogin();
  }

  ngOnInit() {
    
  }

  onSubmit() {
    this.loginService.login(this.user)
    .subscribe(res => {
        if (res != null) {
          sessionStorage.setItem("userLog", res);
          sessionStorage.setItem("statusLog", "true");
          this.router.navigate(['/manager']);
        } else {
          sessionStorage.setItem("statusLog", "false");
          this.router.navigate(['/login']);
        }
       
      });
  }

}