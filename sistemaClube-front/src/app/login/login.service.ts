import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { path } from '../constants';

import { Http, Response } from '@angular/http';
import 'rxjs/Rx';


@Injectable()
export class LoginService {

  private headers: Headers; 
  private cors;

  constructor(private router: Router, private http: Http) {
    this.headers = new Headers({
      "Allow-Control-Allow-Origin": "*",
      "Allow-Control-Allow-Methods": "POST, GET, PUT, DELETE",
      "Content-Type": "application/json"
    });
    this.cors = {headers: this.headers}

  }
  
  login(user: any) {
    return this.http.post(`${path}/e/validation`, user, this.cors)
    .map((res: Response) => res.json());
  }

}