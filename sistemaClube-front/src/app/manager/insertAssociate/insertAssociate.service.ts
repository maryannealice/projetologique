import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { path } from '../../constants';

@Injectable()
export class InsertAssociateService {

  private headers: Headers;
  private cors;

  constructor(private http: Http) { 
    this.headers = new Headers({
      "Allow-Control-Allow-Origin": "*",
      "Allow-Control-Allow-Methods": "POST, GET, PUT, DELETE",
      "Content-Type": "application/json"
    });
    this.cors = {headers: this.headers};
  }

  insertAssociate(associate: any) {
    return  this.http.post(`${path}/a/insertAssociate`, associate, this.cors)
    .map((res: Response) => res.json())
    .subscribe(res => res.json());
  }

  insertResponsible(responsible: any) {
    return this.http.post(`${path}/r/insertResponsible`, responsible, this.cors)
    .map((res: Response) => res.json())
    .subscribe(res => res.json());
  }

  insertDependent(dependent: any) {
    return this.http.post(`${path}/d/insertDependent`, dependent, this.cors)
    .map((res: Response) => res.json())
    .subscribe(res => res.json());
  }

  listResponsible() {
    return this.http.get(`${path}/r/listResponsible`, this.cors)
    .map((res: Response) => res.json())
    .subscribe(res => {
      console.log( Object.values(res) );
    });
  }

}
