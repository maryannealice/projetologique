import { Component, OnInit } from '@angular/core';
import { Dependent } from '../../models/dependent';
import { Associate } from '../../models/associate';
import { Responsible } from '../../models/responsible';
import { TextMaskModule } from 'angular2-text-mask';
import { InsertAssociateService } from './insertAssociate.service';

@Component({
  selector: 'app-insertAssociate',
  templateUrl: './insertAssociate.component.html',
  styleUrls: ['./insertAssociate.component.css']
})
export class InsertAssociateComponent implements OnInit {

  public maskCpf = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  public maskRg = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/];
  public maskPhone = ['(',/[0-9]/, /\d/,')', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskCellphone = ['(',/[0-9]/, /\d/,')', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskCep = [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];

  state: any[] = [ { uf: 'AC' }, { uf: 'AL' }, { uf: 'AP' }, { uf: 'AM' },
    { uf: 'BA' }, { uf: 'CE' }, { uf: 'DF' }, { uf: 'ES' }, { uf: 'GO' },
    { uf: 'MA' }, { uf: 'MT' }, { uf: 'MS' }, { uf: 'MG' }, { uf: 'PA' },
    { uf: 'PB' }, { uf: 'PR' }, { uf: 'PE' }, { uf: 'PI' }, { uf: 'RJ' },
    { uf: 'RN' }, { uf: 'RS' }, { uf: 'RO' }, { uf: 'RR' }, { uf: 'SC' },
    { uf: 'SP' }, { uf: 'SE' }, { uf: 'TO' }
  ];

  dependent: Dependent;
  associate: Associate;
  responsible: Responsible;

  constructor(private insertAssociateService: InsertAssociateService) {
    this.dependent = new Dependent();
    this.associate = new Associate();
    this.responsible = new Responsible();
  }

  ngOnInit() { 
    this.insertAssociateService.listResponsible();    

    document.getElementById('bank').style.display = "none";
    document.getElementById('dependent').style.display = "none";
  }

   option(value: string) {
    if (value == "responsible") {
      document.getElementById('dependent').style.display = "none";
      document.getElementById('bank').style.display = "block";
    } else if (value == "dependent") {
      document.getElementById('dependent').style.display = "block";
      document.getElementById('bank').style.display = "none";
    }
   }

   onSubmit() {
     console.log("oi");
     this.insertAssociateService.insertAssociate(this.associate);
     
    if (Object.keys(this.responsible).length == 0) {
       this.insertAssociateService.insertDependent(this.dependent);
    } else 
      if (Object.keys(this.dependent).length == 0) {
      this.insertAssociateService.insertResponsible(this.responsible);
    }    
  }

}