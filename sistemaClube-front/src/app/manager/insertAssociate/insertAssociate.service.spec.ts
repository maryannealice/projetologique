import { TestBed, inject } from '@angular/core/testing';

import { InsertAssociateService } from './insertAssociate.service';

describe('InsertAssociateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InsertAssociateService]
    });
  });

  it('should be created', inject([InsertAssociateService], (service: InsertAssociateService) => {
    expect(service).toBeTruthy();
  }));
});
