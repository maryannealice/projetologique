import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertAssociateComponent } from './insertAssociate.component';

describe('BodyComponent', () => {
  let component: InsertAssociateComponent;
  let fixture: ComponentFixture<InsertAssociateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertAssociateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertAssociateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
