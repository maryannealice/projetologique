import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagerComponent } from './manager.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { InsertAssociateComponent } from './insertAssociate/insertAssociate.component';
import { InsertAssociateService } from './insertAssociate/insertAssociate.service';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    TextMaskModule,
  ],
  declarations: [
    ManagerComponent, 
    MenuComponent, 
    InsertAssociateComponent,
    FooterComponent
  ],
  providers: [InsertAssociateService]
})
export class ManagerModule { }
