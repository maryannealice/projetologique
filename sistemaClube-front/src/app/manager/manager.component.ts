import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {
  
  statusLog = sessionStorage.getItem("statusLog");

  constructor(private router: Router) {  }

  ngOnInit() {
    if (this.statusLog != "true") {
      this.router.navigateByUrl("/login");
    }
  }

}